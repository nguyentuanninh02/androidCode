package com.example.myapplication.Entity;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Product_Table")
public class ProductEntity {
    @ColumnInfo(name = "Product_ID")
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "Product_Name")
    @Nullable
    private String name;

    @ColumnInfo(name = "Product_Price")
    private float price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
