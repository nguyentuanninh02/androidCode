package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class DemoMapActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, LocationListener {

    private Location currentLocation;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_map);
        SupportMapFragment supportMapFragment= (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.recycler_view_product_list);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapLoaded() {
        LatLng latLng= new LatLng(21.2303033, 105.202020);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        MarkerOptions markerOptions= new MarkerOptions().position(latLng);
        map.addMarker(markerOptions);

        PolylineOptions polylineOptions= new PolylineOptions();
        polylineOptions.add(latLng);
        LatLng latLng1= new LatLng(21.2303033, 105.202020);
        polylineOptions.add(latLng1);
        map.addPolyline(polylineOptions);

        if(currentLocation!= null){
            latLng1= new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 10));
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        map= googleMap;
        map.setOnMapLoadedCallback(this);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        this.currentLocation= location;
    }

    public void getCurrentLocation(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            //request permisson
        }
        LocationManager locationManager= (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        currentLocation= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }
}
