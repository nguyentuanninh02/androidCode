package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.bean.UserInfo;
import com.example.myapplication.database.DatabaseHandler;
import com.example.myapplication.resolver.DemoUserContentResolver;

public class RegisterUserActivity extends AppCompatActivity {
    private DatabaseHandler databaseHandler= null;
    TextView tvUsername, tvPassword;
    private Spinner spinnerCampus;
    RadioGroup role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        databaseHandler = new DatabaseHandler(this);
        DemoUserContentResolver demoUserContentResolver= new DemoUserContentResolver(this);
        Button btSaveUser= findViewById(R.id.bt_save_user_info);
        tvUsername= findViewById(R.id.edit_user_name);
        tvPassword= findViewById(R.id.edit_password);
        spinnerCampus= findViewById(R.id.edit_campus);

        ArrayAdapter<CharSequence> arrayAdapter= ArrayAdapter.createFromResource(
                this, R.array.campus, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCampus.setAdapter(arrayAdapter);

        role= findViewById(R.id.edit_role);
        role.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

            }
        });

        btSaveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username= tvUsername.getText().toString();
                String password= tvPassword.getText().toString();
                String role= "staff";
                String campus="Ha Noi";
                UserInfo userInfo= new UserInfo();
                userInfo.setUsername(username);
                userInfo.setPassword(password);
                userInfo.setRole(role);
                userInfo.setCampus(campus);

                long result= databaseHandler.insertUser(username, password, role, campus);
                if(result>= 0){
                    Toast.makeText(RegisterUserActivity.this, "Registered user successfully",
                            Toast.LENGTH_SHORT).show();
                }

                Intent intent= new Intent(RegisterUserActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}