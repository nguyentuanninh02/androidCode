package com.example.myapplication.webService;

import com.example.myapplication.bean.PostInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MyApiEndpointInterface {
    @GET("/post")
    Call<List<PostInfo>> getAllPosts();

    @GET("/post/{id}")
    Call<PostInfo> getPostInfo(@Path("id") String id);
}
