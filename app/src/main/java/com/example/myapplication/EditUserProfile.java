package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditUserProfile extends AppCompatActivity {
    EditText etFirstName, etLastName, etMobilePhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);

        etFirstName = findViewById(R.id.et_first_name);
        etLastName = findViewById(R.id.et_last_name);
        etMobilePhone = findViewById(R.id.et_phone);
        Button btSave = findViewById(R.id.button_save);
        Intent intent = getIntent();
        if (intent != null) {
            etFirstName.setText(intent.getStringExtra("FIRST_NAME"));
            etLastName.setText(intent.getStringExtra("LAST_NAME"));
            etMobilePhone.setText(intent.getStringExtra("MOBILE_NO"));
        }
        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        etLastName.setText(etLastName.getText().toString() + ", user name= " +
                sharedPreferences.getString("USER_NAME", ""));
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent();
                intent1.putExtra("FIRST_NAME", etFirstName.getText().toString());
                intent1.putExtra("LAST_NAME", etLastName.getText().toString());
                intent1.putExtra("MOBILE_NO", etMobilePhone.getText().toString());
                setResult(2, intent1);
                finish();
            }
        });
    }
}