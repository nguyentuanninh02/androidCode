package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.Entity.ProductEntity;
import com.example.myapplication.adapter.ProductListAdapter;
import com.example.myapplication.bean.Product;
import com.example.myapplication.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ProductListActivity extends AppCompatActivity {
    private ProductRepository productRepository= null;
    private List<Product> products= new ArrayList<>();
    private ProductListAdapter productListAdapter= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        productRepository= new ProductRepository(this);
        List<ProductEntity> productEntities= productRepository.getAllProduct();
        products.addAll(convertProductObject(productEntities));

        RecyclerView recyclerViewProduct= findViewById(R.id.recycler_view_product_list);
        productListAdapter= new ProductListAdapter(products, this);

        recyclerViewProduct.setAdapter(productListAdapter);

        recyclerViewProduct.setLayoutManager(new LinearLayoutManager(this));

        Button btCreateProduct= findViewById(R.id.bt_create_product);
        btCreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1= new Intent(ProductListActivity.this, CreateProductActivity.class);
                startActivityForResult(intent1,2);
            }
        });
    }
    private void readProductFromFile(){
        Scanner scanner= new Scanner(getResources().openRawResource(R.raw.product_list));
        while (scanner.hasNextLine()){
            Product product= new Product();
            String line= scanner.nextLine();
            StringTokenizer stringTokenizer= new StringTokenizer(line, ", ");
            if(stringTokenizer.hasMoreTokens()){
                product.setId(stringTokenizer.nextToken());
            }
            if(stringTokenizer.hasMoreTokens()){
                product.setName(stringTokenizer.nextToken());
            }
            if(stringTokenizer.hasMoreTokens()){
                try{
                    product.setPrice(Float.parseFloat(stringTokenizer.nextToken()));
                }catch (Exception ex){
                    Log.d(getClass().getSimpleName(), "Exception: "+ ex.toString());
                }
            }
            products.add(product);
        }
    }

    private void getProducts(){

        Random random= new Random(1000);
        for(int i= 0; i< 50; ++i){
            Product product= new Product("00"+ i, "product "+ i, random.nextFloat());
            products.add(product);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode== 2){
            products.clear();
            List<ProductEntity> productEntities= productRepository.getAllProduct();
            products.addAll(convertProductObject(productEntities));
            productListAdapter.notifyDataSetChanged();
        }
    }

    private List<Product> convertProductObject(List<ProductEntity> productEntities){
        if(productEntities== null){
            return null;
        }
        List<Product> productList= new ArrayList<>();
        for (ProductEntity productEntity: productEntities) {
            productList.add(new Product(""+productEntity.getId(), productEntity.getName(), productEntity.getPrice()));
        }
        return productList;
    }
}