package com.example.myapplication;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.Manifest;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.service.MyDemoBoundService;
import com.example.myapplication.service.MyDemoUnBoundService;
import com.squareup.picasso.Picasso;

public class activity_user_profile extends AppCompatActivity {

    private static final int GPS_REQUEST_CODE = 2;
    private ActivityResultLauncher activityResultLauncher= registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode()== 2){
                        Intent intent= result.getData();
                        tvFirstName.setText(intent.getStringExtra("FIRST_NAME"));
                        tvLastName.setText(intent.getStringExtra("LAST_NAME"));
                        tvMobilePhone.setText(intent.getStringExtra("MOBILE_NO"));
                    }
                }
            }
    );
    private TextView tvMobilePhone;
    private Button btnEditProfile;
    TextView tvFirstName, tvLastName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Intent intent= getIntent();
        String username= null;
        String role= null;
        if(intent!= null){
            username= intent.getStringExtra("USER_NAME");
            intent.getStringExtra("ROLE");
        }
        tvFirstName= findViewById((R.id.fist_name));
        registerForContextMenu(tvFirstName);// register context menu
        tvFirstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu= new PopupMenu(activity_user_profile.this, view);
                Menu menu= popupMenu.getMenu();
                popupMenu.getMenuInflater().inflate(R.menu.menu_context, menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if(menuItem.getItemId()== R.id.menu_delete){
                            showDelete();
                        } else if (menuItem.getItemId()== R.id.menu_edit) {

                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        tvLastName= findViewById((R.id.last_name));
        tvFirstName.setText(username);

        tvMobilePhone= findViewById(R.id.tv_phone);
        tvMobilePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri= Uri.parse("tel: "+ tvMobilePhone.getText().toString());
                Intent it = new Intent(Intent.ACTION_DIAL,uri);
                startActivity(it);
            }
        });

        btnEditProfile= findViewById(R.id.bt_edit_profile);
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1= new Intent(activity_user_profile.this, EditUserProfile.class);
                intent1.putExtra("FIRST_NAME", tvFirstName.getText().toString());
                intent1.putExtra("LAST_NAME", tvLastName.getText().toString());
                intent1.putExtra("MOBILE_NO", tvMobilePhone.getText().toString());
                startActivityForResult(intent1,2);
//                activityResultLauncher.launch(intent1);
            }
        });

        ImageView imageView= findViewById(R.id.imageAvt);
        imageView.setImageResource(R.drawable.img);
        String imgURL="https://cc-prod.scene7.com/is/image/CCProdAuthor/adobe-firefly-marquee-text-to-image-0-desktop-1000x1000?$pjpeg$&jpegSize=300&wid=1000";
        Picasso.with(this).load(imgURL).placeholder(R.drawable.img).error(R.drawable.img).into(imageView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode== 2){
            tvFirstName.setText(intent.getStringExtra("FIRST_NAME"));
            tvLastName.setText(intent.getStringExtra("LAST_NAME"));
            tvMobilePhone.setText(intent.getStringExtra("MOBILE_NO"));
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        getMenuInflater().inflate(R.menu.menu_context, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()== R.id.menu_delete){
            showDelete();
        } else if(item.getItemId()== R.id.menu_edit){

        }
        return true;
    }

    private void showDelete(){
        Toast.makeText(this, "Delete menu is selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        if(menuItem.getItemId()== R.id.option_setting){
            showSetting();
        } else if(menuItem.getItemId()== R.id.option_favourite){
            showFavourite();
        } else if(menuItem.getItemId()== R.id.option_gps){
            requestGPS();
        } else if(menuItem.getItemId()== R.id.option_notification){
            sendNotification();
        }else if(menuItem.getItemId()== R.id.option_start_service){
            startNotificationService();
        }else if(menuItem.getItemId()== R.id.option_show_count){
            showCount();
        }
        return super.onOptionsItemSelected(menuItem);
    }
    private MyDemoBoundService myDemoBoundService= null;
    private void showCount(){
        Toast.makeText(this, "Count got from service "+
                myDemoBoundService.getCount(), Toast.LENGTH_SHORT).show();
    }

    private ServiceConnection serviceConnection= new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            myDemoBoundService= ((MyDemoBoundService.MyBinder) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent= new Intent(this, MyDemoBoundService.class);
        intent.putExtra("COUNT", 5);
        bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(serviceConnection);
    }

    private void startNotificationService(){
        Intent intent= new Intent(this, MyDemoUnBoundService.class);
        intent.putExtra("MSG", "This is notification service");
        startService(intent);
    }

    private void sendNotification(){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.TIRAMISU){
            //request permisstion
        }
        final String CHANNEL_ID= "001";
        NotificationCompat.Builder builder= new NotificationCompat.Builder(this,
                CHANNEL_ID).setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_action_setting)
                .setContentTitle("Demo notification")
                .setContentText("Notification text")
                .setAutoCancel(false);
        Intent intent= new Intent(this, NotificationDetailActivity.class);
        intent.putExtra("NSG", "Hello how are you");
        PendingIntent pendingIntent= PendingIntent.getActivity(this, 2, intent, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=
                    new NotificationChannel(CHANNEL_ID, "Demo channel",
                            NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(2, builder.build());
    }

    private void requestGPS(){
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED){
            if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                AlertDialog.Builder builder= new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setTitle("Explain Request GPS")
                        .setMessage("Please grant permission for GPS service")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION}, GPS_REQUEST_CODE);
                            }
                        });
                builder.show();
            } else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, GPS_REQUEST_CODE);
            }
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permission, int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permission, grantResults);
        switch (requestCode){
            case GPS_REQUEST_CODE:
                if(grantResults.length> 0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "GPS is granted", Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(this, "GPS did not grant permission for GPS Service", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void showSetting(){
        Toast.makeText(this, "setting is selected", Toast.LENGTH_SHORT).show();
    }
    public void showFavourite(){
        Toast.makeText(this, "favourite is selected", Toast.LENGTH_SHORT).show();
    }
}