package com.example.myapplication.provider;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;

public class DemoUserContract {
    public class UserTable {
        public final static String USER_TABLE = "USER_INFO";
        public final static String USER_NAME = "USER_NAME";
        public final static String PASSWORD = "PASSWORD";
        public final static String ROLE = "ROLE";
        public final static String CAMPUS = "CAMPUS";
    }

    public final static String AUTHORITY = "com.example.myapplication.provider.DemoUserContentProvider";
    public final static String CONTENT_PATH = UserTable.USER_TABLE;
    public final static Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + CONTENT_PATH);
    public final static String ONE_USER_MINE_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/mt_user";
    public final static String ALL_USER_MINE_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/mt_user";
    public final static int ONE_USER = 1;
    public final static int ALL_USER = 2;
    public static UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, CONTENT_PATH + "/", ONE_USER);
        URI_MATCHER.addURI(AUTHORITY, CONTENT_PATH, ALL_USER);
    }

}
