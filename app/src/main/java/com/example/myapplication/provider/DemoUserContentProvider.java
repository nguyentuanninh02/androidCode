package com.example.myapplication.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.database.DatabaseHandler;

public class DemoUserContentProvider extends ContentProvider {
    private DatabaseHandler databaseHandler= null;
    @Override
    public boolean onCreate() {
        databaseHandler= new DatabaseHandler(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        Cursor cursor= null;
        String tableName= uri.getLastPathSegment();
        switch (DemoUserContract.URI_MATCHER.match(uri)){
            case DemoUserContract.ONE_USER:
                cursor= databaseHandler.getReadableDatabase().query(tableName, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case DemoUserContract.ALL_USER:
                cursor= databaseHandler.getWritableDatabase().query(tableName, projection,
                        null, null, null, null, sortOrder);
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String minType= null;
        switch (DemoUserContract.URI_MATCHER.match(uri)){
            case DemoUserContract.ONE_USER:
                minType= DemoUserContract.ONE_USER_MINE_TYPE;
                break;
            case DemoUserContract.ALL_USER:
                minType= DemoUserContract.ALL_USER_MINE_TYPE;
                break;
        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        String table= uri.getLastPathSegment();
        Uri uri1= Uri.parse("content://"+ DemoUserContract.AUTHORITY+"/"+ table+"/"
        +contentValues.get(DemoUserContract.UserTable.USER_NAME));

        databaseHandler.getWritableDatabase().insert(table,null, contentValues);
        return uri1;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
