package com.example.myapplication.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class MyDemoBoundService extends Service {
    private int count;
    public class MyBinder extends Binder{
        public MyDemoBoundService getService(){
            return MyDemoBoundService.this;
        }
    }

    private MyBinder myBinder= new MyBinder();
    public MyDemoBoundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        count= intent.getIntExtra("COUNT", 0);
        count++;
        return myBinder;
    }

    public int getCount(){
        return count;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}