package com.example.myapplication.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.example.myapplication.NotificationDetailActivity;
import com.example.myapplication.R;

public class MyDemoUnBoundService extends Service {
    public MyDemoUnBoundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String msg= intent.getStringExtra("MSG");
        sendNotification(msg);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void sendNotification(String msg){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.TIRAMISU){
            //request permisstion
        }
        final String CHANNEL_ID= "001";
        NotificationCompat.Builder builder= new NotificationCompat.Builder(this,
                CHANNEL_ID).setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_action_setting)
                .setContentTitle("Demo notification")
                .setContentText("Notification text "+ msg)
                .setAutoCancel(false);
        Intent intent= new Intent(this, NotificationDetailActivity.class);
        intent.putExtra("NSG", "Hello how are you");
        PendingIntent pendingIntent= PendingIntent.getActivity(this, 2, intent, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=
                    new NotificationChannel(CHANNEL_ID, "Demo channel",
                            NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(2, builder.build());
    }
}