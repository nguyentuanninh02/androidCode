package com.example.myapplication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.fragment.DemoProductListFragment;


public class DemoProductFragmentActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_demo_product_activity);
        DemoProductListFragment demoProductListFragment1= new DemoProductListFragment();
        DemoProductListFragment demoProductListFragment2= new DemoProductListFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.fragment_container_view_tag1, demoProductListFragment1)
                .add(R.id.fragment_container_view_tag2, demoProductListFragment2)
                .commit();
    }
}