package com.example.myapplication.resolver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import com.example.myapplication.bean.UserInfo;
import com.example.myapplication.provider.DemoUserContract;

public class DemoUserContentResolver {
    private Context context;
    public DemoUserContentResolver(Context context){
        this.context= context;
    }

    public UserInfo getUserInfo(String username){
        UserInfo userInfo= null;
        Uri uri= Uri.parse("content://"+DemoUserContract.AUTHORITY+ "/"+ DemoUserContract.CONTENT_PATH+"/"+
                username);
        Cursor cursor= context.getContentResolver().query(uri, null,
                DemoUserContract.UserTable.USER_NAME+"=?", new String[]{username}, null, null);
        if(cursor!= null&& cursor.moveToNext()){
            int index= cursor.getColumnIndex(DemoUserContract.UserTable.PASSWORD);
            userInfo= new UserInfo();
            userInfo.setPassword(cursor.getString(index));
            index= cursor.getColumnIndex(DemoUserContract.UserTable.ROLE);
            userInfo.setRole(cursor.getString(index));
            index= cursor.getColumnIndex(DemoUserContract.UserTable.CAMPUS);
            userInfo.setCampus(cursor.getString(index));
            userInfo.setUsername(username);
        }
        return userInfo;
    }
    public void createUser(UserInfo userInfo){
        ContentValues contentValues= new ContentValues();
        //put data into contentValues
        contentValues.put(DemoUserContract.UserTable.USER_NAME, userInfo.getUsername());
        contentValues.put(DemoUserContract.UserTable.PASSWORD, userInfo.getPassword());
        contentValues.put(DemoUserContract.UserTable.ROLE, userInfo.getRole());
        contentValues.put(DemoUserContract.UserTable.CAMPUS, userInfo.getCampus());
        Uri uri= DemoUserContract.CONTENT_URI;
        uri= context.getContentResolver().insert(uri, contentValues);
        if(uri!= null && uri.getLastPathSegment().equals(userInfo.getUsername())){
            Toast.makeText(context, "Create user successfully", Toast.LENGTH_SHORT).show();
        }
    }
}
