package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.Entity.ProductEntity;
import com.example.myapplication.repository.ProductRepository;

public class CreateProductActivity extends AppCompatActivity {
    private ProductRepository productRepository= null;
    EditText etProductName, etProductPrice;
    Button btSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);

        etProductName= findViewById(R.id.et_product_name);
        etProductPrice= findViewById(R.id.et_product_price);
        btSave= findViewById(R.id.bt_save_product);

        productRepository= new ProductRepository(this);

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductEntity productEntity= new ProductEntity();
                productEntity.setName(etProductName.getText().toString());
                productEntity.setPrice(Float.parseFloat(etProductPrice.getText().toString()));

                productRepository.createProduct(productEntity);

                Intent intent= new Intent(CreateProductActivity.this, ProductListActivity.class);
                startActivity(intent);
            }
        });

    }
}