package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.myapplication.bean.PostInfo;
import com.example.myapplication.webService.MyApiEndpointInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostListActivity extends AppCompatActivity {

    private List<PostInfo> postInfo= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);
        retrievePost();
    }

    private void retrievePost(){
        String baseURL= "https://jsonplaceholder.typicode.com";
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();
        MyApiEndpointInterface myApiEndpointInterface= retrofit.create(MyApiEndpointInterface.class);
        myApiEndpointInterface.getAllPosts().enqueue(new Callback<List<PostInfo>>() {
            @Override
            public void onResponse(Call<List<PostInfo>> call, Response<List<PostInfo>> response) {
                postInfo.addAll(response.body());
            }

            @Override
            public void onFailure(Call<List<PostInfo>> call, Throwable t) {
                Log.d(getClass().getSimpleName(), "Get post error");
            }
        });
    }
}