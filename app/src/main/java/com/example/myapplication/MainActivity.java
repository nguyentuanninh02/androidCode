package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner spinnerCampus;
    private CheckBox chkRemember;
    private EditText edtUsername;
    private String role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerCampus= findViewById(R.id.edit_campus);
        ArrayAdapter<CharSequence> arrayAdapter= ArrayAdapter.createFromResource(
                this, R.array.campus, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCampus.setAdapter(arrayAdapter);
        spinnerCampus.setOnItemSelectedListener(this);
        chkRemember= findViewById(R.id.remember);

        RadioButton radioButtonStaff= findViewById(R.id.role_staff);
        radioButtonStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                role= "Staff";
            }
        });

        edtUsername= findViewById(R.id.edit_user_name);
        Button btnLogin= findViewById(R.id.login);

        SharedPreferences sharedPreferences= getSharedPreferences("UserInfo", Context.MODE_PRIVATE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Login button is not check", Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(MainActivity.this, activity_user_profile.class);
                intent.putExtra("USER_NAME", edtUsername.getText().toString());
                intent.putExtra("ROLE", role);
                SharedPreferences.Editor edit= sharedPreferences.edit();
                edit.putString("USER_NAME", edtUsername.getText().toString());
                edit.putString("ROLE", role);
                edit.commit();
                startActivity(intent);
            }

        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String selectedItem= parent.getItemAtPosition(position).toString();
        Toast.makeText(this, "Selected campus: "+ selectedItem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}