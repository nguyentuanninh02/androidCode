package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.Entity.ProductEntity;

import java.util.List;

@Dao
public interface ProductDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ProductEntity product);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(ProductEntity product);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(ProductEntity ...productEntities);

    @Query("SELECT * FROM Product_Table p WHERE p.Product_ID =:id")
    ProductEntity select(int id);

    @Query("SELECT * FROM Product_Table")
    List<ProductEntity> selectAll();
}
