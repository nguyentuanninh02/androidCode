package com.example.myapplication.repository;

import android.content.Context;

import com.example.myapplication.Entity.ProductEntity;
import com.example.myapplication.dao.ProductDAO;
import com.example.myapplication.dao.ProductRoomDatabase;

import java.util.List;

public class ProductRepository {
    private ProductDAO productDAO;
    public ProductRepository(Context context){
        ProductRoomDatabase productRoomDatabase= ProductRoomDatabase.getInstance(context);
        productDAO= productRoomDatabase.productDAO();
    }

    public void createProduct(ProductEntity product){
        productDAO.insert(product);
    }
    public ProductEntity getProduct(int id){
        return productDAO.select(id);
    }

    public List<ProductEntity> getAllProduct(){
        return productDAO.selectAll();
    }
}
