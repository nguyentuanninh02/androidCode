package com.example.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.ProductDetailActivity;
import com.example.myapplication.R;
import com.example.myapplication.bean.Product;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {
    private List<Product> products;
    private Context context;
    public ProductListAdapter(List<Product> products, Context context){
        this.context= context;
        this.products= products;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.product_list_item, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product= products.get(position);
        holder.tvProductId.setText(product.getId());
        holder.tvProductName.setText(product.getName());
        holder.tvProductPrice.setText(""+product.getPrice());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View

            .OnClickListener{
        private TextView tvProductId, tvProductName, tvProductPrice;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProductId= itemView.findViewById(R.id.tv_product_id);
            tvProductName= itemView.findViewById(R.id.tv_product_name);
            tvProductPrice= itemView.findViewById(R.id.tv_product_price);
            tvProductName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent= new Intent(context, ProductDetailActivity.class);
            intent.putExtra("PRODUCT_ID", tvProductId.getText());
            context.startActivity(intent);
        }
    }
}
