package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myapplication.bean.Product;

public class ProductDetailActivity extends AppCompatActivity {

    TextView tvProductId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Intent intent= getIntent();
        String productId= intent.getStringExtra("PRODUCT_ID");

        tvProductId= findViewById(R.id.tv_product_id);
        tvProductId.setText(productId);
    }
}