package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.myapplication.bean.UserInfo;

public class DatabaseHandler extends SQLiteOpenHelper {
    public final static int DB_VERSION=3;
    public final static String DB_NAME ="UserData";
    public final static String USER_TABLE = "USER_INFOR";
    public final static String USER_NAME="USER_NAME";
    public final static String PASSWORD = "PASSWORD";
    public final static String ROLE ="ROLE";
    public final static String CAMPUS = "CAMPUS";
    public final static String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS "+ USER_TABLE + "("
            +USER_NAME+ " TEXT NOT NULL PRIMARY KEY, "+ PASSWORD+ " TEXT, "+
            ROLE+ " TEXT, "+ CAMPUS + " TEXT )";

    public DatabaseHandler(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        if(sqLiteDatabase.isOpen()){
            sqLiteDatabase.execSQL(CREATE_USER_TABLE);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        if( sqLiteDatabase.isOpen()){

            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ USER_TABLE);

            sqLiteDatabase.execSQL(CREATE_USER_TABLE);
        }
    }
    public  long  insertUser(String username, String password, String role, String campus){
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_NAME, username);
        contentValues.put(PASSWORD, password);
        contentValues.put(ROLE, role);
        contentValues.put(CAMPUS, campus);
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insertOrThrow(USER_TABLE, null, contentValues);
    }
    public UserInfo selectUser(String username){
        UserInfo userInfo = null;
        String selectUser= "SELECT* FROM " + USER_TABLE+" WHERE " + USER_NAME + " =? ";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor= sqLiteDatabase.rawQuery(selectUser, new String[]{username});
        if(cursor.moveToNext()){
            userInfo= new UserInfo();
            userInfo.setUsername(username);
            int index = cursor.getColumnIndex(PASSWORD);
            userInfo.setPassword(cursor.getString(index));
            index= cursor.getColumnIndex(ROLE);
            userInfo.setRole(cursor.getString(index));
            index= cursor.getColumnIndex(CAMPUS);
            userInfo.setCampus(cursor.getString(index));
        }
        return userInfo;
    }
}